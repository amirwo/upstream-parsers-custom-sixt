name := "upstream-parsers-custom-sixt"

organization := "com.upstreamsecurity"

scalaVersion := "2.12.8"

val localBranchName = System.getProperty("teamcity.build.branch", "local-develop").split("/").last
val isLocal = localBranchName == "local-develop"
val buildNumber = sys.props.getOrElse("build.number", default = "0").concat(if(isLocal) "-SNAPSHOT" else "")
val versionStr = s"1.0.$buildNumber"
version := versionStr

val branchName = System.getProperty("teamcity.build.branch", "develop").split("/").last
val repositoryName = if (branchName == "master") "sbt-release-local" else "sbt-dev-local"
val repository = s"https://artifactory.upstreamsecurity.io/artifactory/$repositoryName"

resolvers ++= Seq(
  "Artifactory" at repository,
  "confluent" at "http://packages.confluent.io/maven/"
)

publishTo := Some("Artifactory Realm" at repository)
credentials += Credentials("Artifactory Realm", "artifactory.upstreamsecurity.io", "teamcity", "AP6WjvN6ExvkuQ29ZPvtSECDy1e")

homepage := Some(url("https://bitbucket.org/upstreamsecurity/upstream-parsers-custom-sixt"))
scmInfo := Some(
  ScmInfo(
    url("https://bitbucket.org/upstreamsecurity/upstream-parsers-custom-sixt"),
    "scm:git@bitbucket.org:upstreamsecurity/upstream-parsers-custom-sixt.git",
    Some("scm:git@bitbucket.org:upstreamsecurity/upstream-parsers-custom-sixt.git")
  )
)

scalacOptions := Seq(
  "-deprecation",
  "-encoding", "UTF-8",
  "-feature",
  "-target:jvm-1.8",
  "-unchecked",
  "-Xfuture",
  "-Xlint",
  "-Yrangepos",
  "-Ywarn-dead-code",
  "-Ywarn-nullary-unit",
  "-Ywarn-unused",
  "-Ywarn-unused-import"
)

publishArtifact in packageDoc := false

libraryDependencies ++= {
  val upstreamVersion  = if (buildNumber == "0" && branchName == "master") "1.0.0" else if(isLocal) versionStr else "1.0.+"
  val spec2Version     = "3.9.5"

  Seq(
    "org.slf4j"             % "slf4j-api"             % "1.7.26",
    "com.upstreamsecurity" %% "upstream-domain-logic" % upstreamVersion,
    // test dependencies
    "org.specs2"           %% "specs2-core"           % spec2Version  % "test"
  )
}


updateOptions := updateOptions.value.withLatestSnapshots(false)
